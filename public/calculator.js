/*
Javascript to:
add and remove rows from the table,
calculate the weighted average
calculate the mean
update percentage column dynamically
*/

const fieldCount = 3;
var rowCount = 4, maxRow = 15, minRow = 1;

// Helper Function: Returns an array of values from all input fields
// Array is organized in triples to be later accessed reliably
// Returns an error if numbers are out of bounds or not numbers at all
function getInput() {

    var input = [];

    for (i=1; i<=rowCount; i++) {
      for (j=1; j<=fieldCount; j++) {
        input.push(document.getElementById("A" + i + j).value);
      }
    }

    try {
      for (i=0; i<rowCount; i++) {
        if (isNaN(input[fieldCount*i]) || input[fieldCount*i] <= 0 || input[fieldCount*i] >= 100)
          throw "Weights must be numbers between zero and one hundred exclusive.";
        for (j=1; j<=2; j++) {
        if (input[fieldCount*i + j] == "" || isNaN(input[fieldCount*i + j]) || input[fieldCount*i + j] < 0 || input[fieldCount*i + j] > 500)
          throw "Grades must be numbers between zero and 500 inclusive.";
        }
        if (input[fieldCount*i + 2] == 0)
          throw "Grade denominator leads to divide by 0 error"
        if (input[fieldCount*i + 1] > input[fieldCount*i + 2])
          throw "Grade numerators must not exceed denominators.";
      }
    } catch (err){
        alert(err);
        return null;
    }

    return input;
}


// Function which returns the percentage to 1 rounded decimal place
// Called when a user inputs into any input field
// Updates percentage with N/A if there are errors
function Percentage(rowNum) {
  var num = document.getElementById("A" + rowNum + 2).value;
  var denom = document.getElementById("A" + rowNum + 3).value;

  try {
    if (isNaN(num) || isNaN(denom) || num < 0 || num > denom || denom <= 0) throw ""
  } catch(err) {
    return "N/A"
  }

  return (Math.round( (num/denom * 100) * 10 ) / 10) + "%";
}

// Function to govern action when a user presses the add Row or remove Row buttons
// Uses the buttonId to determine action
// Follows a naming scheme so that input fields in new rows can be accesed by id
//  and so that percentage can be updated appropriately
function addRemoveRow(buttonId) {

    var table = document.getElementById("ActivityTable");

    if(buttonId == addRow) {

      try {
        if (rowCount == maxRow) throw "The maximum number of rows is " + maxRow + "."

        rowCount++;
        var row = table.insertRow(rowCount);

        var cell = [];
        for (i=0; i<5; i++) {
          cell.push(row.insertCell(i));
          var cellId1, cellId2;
          switch (i) {
            case(0):
              cell[i].innerHTML = "Activity " + rowCount
              break;
            case(1):
              cell[i].innerHTML = "A" + rowCount
              break;
            case(2):
              cellId1 = "A"+rowCount+(i-1)
              cell[i].innerHTML = "<input id=" + cellId1 + ">"
              break;
            case(3):
              cellId1 = "A"+rowCount+(i-1)
              cellId2 = "A"+rowCount+(i)
              cellId3 = "P" + rowCount
              cell[i].innerHTML = "<input id=" + cellId1 + " oninput='document.getElementById(\"" + cellId3
                + "\").innerHTML=Percentage(" + rowCount + ")'> / <br> <input id=" + cellId2
                + " oninput='document.getElementById(\"" + cellId3
                + "\").innerHTML=Percentage(" + rowCount + ")'>"
              break;
            case(4):
              cell[i].id = "P" + rowCount
              cell[i].innerHTML = ""
              break;
          }
        }

      }
      catch (err) {
        alert(err);
      }

    } else {

      try {
        if (rowCount == 1) throw "The minimum number of rows is " + minRow + "."
        table.deleteRow(rowCount);
        rowCount--;
      }
      catch (err) {
        alert(err);
      }

    }
}

// Calculates the weighted average of all fields
// Calls getInput() function which handles errors and uses its return to calculate
//   the correct result.
function Weighted() {
  input = getInput();
  if (input == null) {
    return "Error";
  }

  var weighted = 0, weightSum = 0;
  for (i=0; i<input.length; i+=3){
    weighted += parseInt(input[i]) * ( parseInt(input[i+1]) / parseInt(input[i+2]) );
    weightSum += parseInt(input[i]);
  }

  return "Weighted Grade: " + (Math.round( ((weighted/weightSum) * 100) * 10 ) / 10) + "/100";
}

// Calculates the mean of all fields
// Calls getInput() function which handles errors and uses its return to calculate
//   the correct result.
function Mean() {
  input = getInput();
  if (input == null) {
    return "Error";
  }

  var mean = 0, count = 0;
  for (i=0; i<input.length; i+=3){
    mean += ( parseInt(input[i+1]) / parseInt(input[i+2]) );
    count++;
  }

  return "Mean Grade: " + (Math.round( ((mean/count) * 100) * 10 ) / 10) + "/100";
}
